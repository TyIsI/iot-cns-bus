$( document ).ready( function() {
  $("time.timeago").timeago();
  $(".action-post").on( 'click', function() {
	  var params = {
			  url : $( this ).attr('action'),
			  type : 'POST',
			  dataType : "json",
			  success : function( response ) {
				  window.location.reload();
			  }
	  };
	  if( $( this ).data() ) params.data = $( this ).data();
	  $.ajax( params );
  });
  $(".action-put").on( 'click', function() {
	  var params = {
		  url : $( this ).attr('action'),
		  type : 'POST',
		  dataType : "json",
		  success : function( response ) {
			  window.location.reload();
		  }
	  };
	  if( $( this ).data() ) params.data = $( this ).data();
	  $.ajax( params );
  });
  $(".action-delete").on( 'click', function() {
	  var result = confirm( "Are you sure?" );
	  if( result ) {
		  var params = {
				  url : $( this ).attr('action'),
				  type : 'DELETE',
				  dataType : "json",
				  success : function( response ) {
					  window.location.reload();
				  }
		  }
		  if( $( this ).data() ) params.data = $( this ).data();
		  $.ajax( params );
	  }
  });
});