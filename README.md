# IoT CNS Bus

## Installing

### Running

`npm install --production`

`bower install`

`npm start`

### Development

`npm install`

`bower install`

`export DEBUG="iot-cns-bus" ; npm start`

## Endpoints

### / (GET)

Get the overview control panel

### /all/queue/clear (DELETE)

Clear all the queues for both brain and limbs

### /brain/queue/clear/:limb (DELETE)

Clear messages from the brain queue for the specified brain

### /brain/queue/clear/all (DELETE)

Clear the brain queue

### /brain/queue/get (GET)

Get a message from the brain queue

### /brain/queue/send/:limb (PUT)

Send a message to the specified limb

### /brain/queue/send/all (PUT)

Broadcast a message to all limbs

### /limbs/all/delete (DELETE)

Delete all limbs

### /limbs/all/get (GET)

Get all limbs

### /limbs/all/queue/clear (DELETE)

Delete all outgoing messages for all limbs

### /limbs/:limb/delete (DELETE)

Delete specified limb

### /limbs/:limb/get (GET)

Get info for specified limb

### /limbs/:limb/queue/clear (DELETE)

Delete all outgoing messages the specified limb

### /limbs/:limb/queue/get (GET)

Get new message from the outgoing message queue for the specified limb

### /limbs/:limb/queue/send (PUT)

Send a message from the specified limb

### /limbs/:limb/status/clear/:key (DELETE)

Clear the specified status key for the specified limb

### /limbs/:limb/status/clear/all (DELETE)

Clear all the status keys for the specified limb

### /limbs/:limb/status/get (GET)

Get the status of the specified limb

The limb device could use this to restore it's status after a reboot.

### /limbs/:limb/status/update (POST)

Update the status for the specified limb

### /time/get (GET)

Get time for the router, as an alternative to NTP

## Websockets

On a connected websocket, brain messages will not be queued, but instead be sent over the websocket through the 'message' channel.
