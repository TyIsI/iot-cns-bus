#!/bin/sh

echo "Missing test functions - described but not defined:"
cat test/tests.all.js | egrep 'it\(' | cut -f2 -d, | cut -f2 -d' ' | while read line ; do echo "$line " | perl -pe 's/\n$//g' ; egrep -c "var $line = function" test/tests.all.js ; done | sort | awk '{ print $2 "\t" $1 }' | perl -pe 's/^0/FAIL/g;s/^\d/PASS/g'

echo "Untested test functions - defined but not described:"
cat test/tests.all.js | egrep '^var\ .*\ =\ function' | awk '{ print $2 }' | while read line ; do egrep -c "it\(\ \".*\", $line" test/tests.all.js | perl -pe 's/\n$//g' ; echo "\t$line"; done | perl -pe 's/^0/FAIL/g;s/^\d/PASS/g'

echo "Missing route tests - defined but not tested:"
egrep '^router\.' routes/*.js | perl -pe 's/^routes//g;s/\.js//g;s/\:router\.(use|post|get|put|post|delete)\(//g' | cut -f1 -d, | perl -pe 's/\ //g;s/\"//g;' | sed "s/'//g" | sed 's/:limb/test/g;s/index\///g;s/:key/testkey/g;s/\//\\\//g;s/(\r|\n)//g' | sort | while read line; do echo "$line " | perl -pe 's/\n//g' ; egrep -c "baseURL \+ '$line'" test/tests.all.js ; done | perl -pe 's/0$/FAIL/g;s/\d$/PASS/g' | awk '{ print $2 "\t" $1 }'

echo "Missing routes - tested but not defined:"
cat test/tests.all.js | egrep 'url\ :\ baseURL\ \+\ ' | cut -f2 -d"'" | sed 's/\/testkey$/\/:key/g;s/\/test/\/:limb/g' | sort | while read line ; do
 ROUTEFILE=`echo "$line" | cut -f2 -d'/' | perl -pe 's/^$|all|time/index/g' | awk '{ print "routes/" $0 ".js" }'`
 ROUTEURL=`echo "$line" | perl -pe 's/^\/(brain|limbs)\//\//g'`
 echo "$line " | perl -pe 's/\n$//g'
 egrep -c "router..*'$ROUTEURL'" "$ROUTEFILE"
done | perl -pe 's/0$/FAIL/g;s/\d$/PASS/g' | awk '{ print $2 "\t" $1 }'

echo "Missing datastore calls - called but not defined:"
egrep "cnsDB\." routes/*.js | perl -pe 's/^.*cnsDB\.(.*)(\(.*)/$1/g' | while read line ; do
 echo "$line " | perl -pe 's/\n$//g'
 egrep -c "var $line = function" lib/datastore.js
done | perl -pe 's/0$/FAIL/g;s/\d$/PASS/g' | awk '{ print $2 "\t" $1 }'

echo "Missing datastore calls - defined but not called:"
egrep 'var\ .*\ \=\ function' lib/datastore.js  | awk '{ print $2 }' | egrep -v 'loadDataStore|saveDataStore|getLimbIDs|newLimb' | while read line ; do echo "$line " | perl -pe 's/\n$//g' ; egrep "$line" routes/*.js | wc -l | awk '{ print $1 }' ; done | perl -pe 's/0$/FAIL/g;s/\d$/PASS/g' | awk '{ print $2 "\t" $1 }'

echo "Duplicate functions in datastore:"
egrep 'var\ .*\ \=\ function' lib/datastore.js | awk '{ print $2 }' | sort | uniq -c | egrep -vw 1 || echo "None"
