var debug = require('debug')('iot-cns-bus:datastore');
var jsonfile = require('jsonfile');

var BreakException = {};

var datastore = {};
var datastoreFile = "datastore.json";

var loadDataStore = function() {
	jsonfile.readFile( datastoreFile, function(err, obj) {
		if( err ) {
			console.error( err );
		} else {
			datastore = obj;
		}
	});
}

var saveDataStore = function() {
	jsonfile.writeFile(datastoreFile, datastore, function( err ) {
		if( err ) {
			console.error( "writeFile error:" );
			console.error( err );
		}
	})
};

module.exports.save = saveDataStore;

setInterval( saveDataStore, 15000 );

var getSummary = function() {
	let limbs = getLimbIDs();

	var result_set = [];
	limbs.forEach( function( limb ) {
		let limbfo = getLimb( limb );
		
		let limb_result = {
			name : limb,
			queue : limbfo.queue.length,
			last_seen : "Never",
			status : limbfo.status
			
		};
		if( limbfo.last_seen > 0 )
			limb_result.last_seen = new Date(limbfo.last_seen).toISOString();
		
		result_set.push( limb_result );
		
	});
	
	return result_set;
}

module.exports.getSummary = getSummary;

var getAllLimbs = function() {
	debug( "getAllLimbs" );
	
	let limbs = getLimbIDs();

	var result_set = [];
	limbs.forEach( function( limb ) {
		let limbfo = getLimb( limb );
		
		result_set.push( {
			name : limb,
			queue : limbfo.queue.length,
			last_seen : limbfo.last_seen
		} );
	});
	
	return result_set;
}

module.exports.getAllLimbs = getAllLimbs;

var getLimbIDs = function() {
	return Object.keys( datastore );
}

var getLimbMessage = function( limb ) {	
	let limbResult = getLimb( limb );
	
	limbResult.last_seen = Date.now();
	
	if( limbResult.queue !== undefined && limbResult.queue.length > 0 )
		return limbResult.queue.shift();
	
	return false;
}

module.exports.getLimbMessage = getLimbMessage;

// Get a message for the brain(s)
var getBrainMessage = function() {
	let brain = getLimb( "brain" );
	
	let result = false;
	
	if( brain.queue.length > 0 )
		result = brain.queue.shift();
	
	return result;
}

module.exports.getBrainMessage = getBrainMessage;

var getLimb = function( limb ) {
	debug( "getLimb", limb );
	
	if( limb == 'all' )
		return getAllLimbs();

	if( datastore[limb] === undefined )
		return newLimb( limb );
		
	return datastore[limb];
}

module.exports.getLimb = getLimb;

var newLimb = function( limbName ) {
	datastore[limbName] = {};
	datastore[limbName].last_seen = 0;
	datastore[limbName].queue = [];
	
	return datastore[limbName];
}

var deleteLimb = function( limb ) {
	debug( "deleteLimb", limb );
	
	// Delete limb
	if( datastore[limb] !== undefined )
		delete datastore[limb];
	
	// TODO: Remove messages from brain queue 

	return true;
}

module.exports.deleteLimb = deleteLimb;

var clearLimbQueues = function() {
	debug( "clearLimbQueues" );
	
	let limbs = getLimbIDs();
	
	limbs.forEach( function( limb ) {
		debug( "clearLimbQueues", limb );
		if( datastore[limb] === undefined || datastore[limb].queue === undefined )
			console.error( "Missing extremities on [" + limb + "]" );
		datastore[limb].queue = [];
	});
	
	return true;
}

module.exports.clearLimbQueues = clearLimbQueues;

var clearLimbQueue = function( limb ) {
	debug( "clearLimbQueue", limb );
	
	let limbResult = getLimb( limb );
	
	if( limbResult.queue === undefined )
		console.error( "clearLimbQueue", "Missing queue on [" + limb + "]" );
	
	limbResult.queue = [];
	
	return true;
}

module.exports.clearLimbQueue = clearLimbQueue;

var getLimbStatus = function( limb ) {
	let limbResult = getLimb( limb );
	
	if( limbResult.status === undefined )
		limbResult.status = {};
	
	return limbResult.status;
}

module.exports.getLimbStatus = getLimbStatus;

var updateLimbStatus = function( limb, updates ) {
	let limbResult = getLimb( limb );
	
	limbResult.last_seen = Date.now();
	
	if( limbResult.status === undefined )
		limbResult.status = {};
	
	let updateKeys = Object.keys( updates );
	
	if( updateKeys.length > 0 ) {
		debug( "updateLimbStatus", "updating keys" );
		updateKeys.forEach( function( updateKey ) {
			debug( "updateLimbStatus", "setting", updateKey, "=>", updates[updateKey] );
			limbResult.status[updateKey] = updates[updateKey];
		});
		
		if( connectedBrains > 0 ) {
			debug( "updateLimbStatus", "connectedBrains > 0" );
			io.sockets.emit( 'status', { limb : limb, status : limbResult.status } );
		}
		
		return true;
	} else {
		debug( "updateLimbStatus", "missing update keys" );
		return false;
	}
}

module.exports.updateLimbStatus = updateLimbStatus;

var removeAllLimbStatusKeys = function( limb ) {
	let limbResult = getLimb( limb );
	
	limbResult.status = {};
	
	return true;
}

module.exports.removeAllLimbStatusKeys = removeAllLimbStatusKeys;

var removeLimbStatusKey = function( limb, idx ) {
	let limbResult = getLimb( limb );
	
	if( limbResult.status === undefined )
		limbResult.status = {};
	
	debug( "removeLimbStatusKey", limbResult.status );
	
	if( limbResult.status[idx] !== undefined )
		delete limbResult.status[idx];
	
	debug( "removeLimbStatusKey", limbResult.status );
	
	return true;
}

module.exports.removeLimbStatusKey = removeLimbStatusKey;

var processMessageFromLimb = function( limb, message ) {
	debug( "processMessageFromLimb");
	
	let limbResult = getLimb( limb );
	
	debug( "processMessageFromLimb", "connectedBrains", connectedBrains );
	
	if( connectedBrains > 0 ) {
		debug( "processMessageFromLimb", "connectedBrains > 0" );
		io.sockets.emit( 'message', { limb : limb, message : message } );
	} else if( connectedBrains == 0 ) {
		debug( "processMessageFromLimb", "limbResult.queue", limbResult.queue );
		limbResult.queue.push( message );
	} else {
		debug( "processMessageFromLimb", "ruh roh" );
	}
	
	return true;
}

module.exports.processMessageFromLimb = processMessageFromLimb;

var processBrainToLimbMessage = function( limb, message ) {
	let limbResult = getLimb( limb );
	
	limbResult.queue.push( message );
	
	return true;
}

module.exports.processBrainToLimbMessage = processBrainToLimbMessage;

var processBrainBroadcastMessage = function( message ) {
	let limbs = getLimbIDs();
	
	limbs.forEach( function( limb ) {
		datastore[limb].queue.push( message );
	});
	
	return true;
}

module.exports.processBrainBroadcastMessage = processBrainBroadcastMessage;

var clearBrainMessageQueue = function( limb ) {
	
	if( limb !== undefined ) {
		datastore["brain"].queue.forEach( function( message, idx ) {
			if( message.limb == limb )
				datastore["brain"].queue.splice( idx, 1 );
		});
	} else {
		datastore["brain"].queue = [];
	}

};

module.exports.clearBrainMessageQueue = clearBrainMessageQueue;

var clearAllMessageQueues = function() {
	clearBrainMessageQueue();
	clearLimbQueues();
};

module.exports.clearAllMessageQueues = clearAllMessageQueues;


loadDataStore();