var express = require( 'express');
var router = express.Router();

var debug = require( 'debug')( 'iot-cns-bus:route:limbs');

var cnsDB = require( '../lib/datastore');

router.get( '/all/get', function( req, res, next ) {
	debug( '/all/get ' );
	let limbs = cnsDB.getAllLimbs();
	res.send( { result : 'OK', limbs : limbs } ).end();
});

router.get( '/:limb/get', function( req, res, next ) {
	debug( '/get/limb', req.params.limb );
	let limb = cnsDB.getLimb( req.params.limb );
	res.send( { result : 'OK', limb : limb } ).end();
});

router.delete( '/all/delete', function( req, res, next ) {
	debug( '/delete/limb', req.params.limb );
	let limb = cnsDB.deleteLimb( req.params.limb );
	res.send( { result : 'OK', limb : limb } ).end();
});

router.delete( '/:limb/delete', function( req, res, next ) {
	debug( '/delete/limb', req.params.limb );
	let limb = cnsDB.deleteLimb( req.params.limb );
	res.send( { result : 'OK', limb : limb } ).end();
});

router.delete( '/all/queue/clear', function( req, res, next ) {
	debug( 'clearing all queue for limb: ', req.params.limb );
	cnsDB.clearLimbQueues( req.params.limb );
	res.send({result:'OK'}).end();
});

router.delete( '/:limb/queue/clear', function( req, res, next ) {
	debug( 'clearing all queue for limb: ', req.params.limb );
	cnsDB.clearLimbQueue( req.params.limb );
	res.send({result:'OK'}).end();
});


router.get( '/:limb/queue/get', function( req, res, next ) {
	debug( '/queue/get', '=>', req.params.limb );
	
	let result = cnsDB.getLimbMessage( req.params.limb );
	
	if( ! result ) {
		res.send({ result:'NOMSG' }).end();
	} else {		
		res.send({ result:'OK', data : result }).end();
	}
	
});

router.put( '/:limb/queue/send', function( req, res, next ) {
	debug( '/queue/send', '=>', req.params.limb, '=>', req.body );
	
	let result = cnsDB.processMessageFromLimb( req.params.limb, req.body );

	debug( '/queue/send', '=>', result );

	if( result ) {
		res.send({ result:'OK'}).end();
	} else {
		res.send({ result:'ERROR'}).end();
	}
});

router.get( '/:limb/status/get', function( req, res, next ) {
	debug( '/status/get', '=>', req.params.limb );
	
	let result = cnsDB.getLimbStatus( req.params.limb );
	
	if( ! result ) {
		res.send({ result:'ERROR'}).end();
	} else {
		res.send({ result : 'OK', data : result }).end();
	}
});

router.post( '/:limb/status/update', function( req, res, next ) {
	debug( '/status/update', '=>', req.params.limb );
	debug( '/status/update', '=>', req.params.limb, '=>', req.body );
	
	let result = cnsDB.updateLimbStatus( req.params.limb, req.body );
	
	res.send({ result:'OK'});
});

router.delete( '/:limb/status/clear/all', function( req, res, next ) {
	debug( '/status/clear/key/all', '=>', req.params.limb );
	
	let result = cnsDB.removeAllLimbStatusKeys( req.params.limb );
	
	res.send({ result:'OK'});
});

router.delete( '/:limb/status/clear/:key', function( req, res, next ) {
	debug( '/status/clear/key', '=>', req.params.limb );
	
	let result = cnsDB.removeLimbStatusKey( req.params.limb, req.params.key );
	
	res.send({ result:'OK'});
});

module.exports = router;
