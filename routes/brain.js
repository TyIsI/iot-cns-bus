var debug = require('debug')('iot-cns-bus:route:brain');
var express = require('express');
var router = express.Router();

var cnsDB = require('../lib/datastore');

router.get( '/queue/get', function( req, res, next ) {
	debug( '/queue/get' );
	let result = cnsDB.getBrainMessage();

	if( ! result ) {
		res.send({ result:'NOMSG' }).end();
	} else {		
		res.send({ result:'OK', data : result }).end();
	}
});

router.delete( '/queue/clear/all', function( req, res, next ) {
	debug( '/queue/clear/', 'all', cnsDB.clearBrainMessageQueue() );
	res.send({ result : 'OK' }).end();
});

router.delete( '/queue/clear/:limb', function( req, res, next ) {
	debug( '/queue/clear/', 'all', req.params.limb, cnsDB.clearBrainMessageQueue( req.params.limb ) );
	res.send({ result : 'OK' }).end();
});

router.put( '/queue/send/all', function( req, res, next ) {
	debug( '/queue/send/', 'all', '=>', req.body );
	cnsDB.processBrainBroadcastMessage( req.body );
	res.send({ result : 'OK' }).end();
});

router.put( '/queue/send/:limb', function( req, res, next ) {
	debug( '/queue/send/', req.params.limb, '=>', req.body );
	cnsDB.processBrainToLimbMessage( req.params.limb, req.body );
	res.send({ result : 'OK' }).end();
});

module.exports = router;
