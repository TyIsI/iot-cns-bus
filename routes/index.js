var debug = require( 'debug')( 'iot-cns-bus:route:index');

var express = require( 'express');
var router = express.Router();

var cnsDB = require( '../lib/datastore');

/* GET home page. */
router.get( '/', function( req, res, next ) {
	debug( '/' );
	res.locals.messages = cnsDB.getSummary();
	res.render( 'index', { title: 'Overview' });
});

router.get( '/time/get', function( req, res, next ) {
	debug( '/time/get' );
	res.send({result:'OK',time:(Math.floor(Date.now()/1000))}).end();
});

router.delete( '/all/queue/clear', function( req, res, next ) {
	debug( '/all/queue/clear', cnsDB.clearAllMessageQueues() );
	res.send({result:'OK'}).end();
});


module.exports = router;
