// var app = require('../app');
var debug = require('debug')('iot-cns-bus:tests');
// var http = require('http');
var request = require('request-promise');
var getLine = require('../lib/utils').getLine;

// Bootstrap the server
// var port = 3000;
// app.set('port', port);
// var server = http.createServer(app);
// server.listen(port);
// server.on('listening', doTests);

var baseURL = 'http://localhost:3000';

var websocket_options ={
		transports: ['websocket'],
		'force new connection': true
};

// Define the tests
var testGetRoot = function( done ) {
	let request_options = {
			url : baseURL + '/',
			resolveWithFullResponse: true
	};
	request.get( request_options )
	.then( function( response ) {
		if( response.statusCode != 200 ) {
			debug( response );
			done( new Error( "Invalid response: " + response.statusCode ) );
			return false;
		}
		if( response.headers['content-type'].substring( 0, "text/html".length ) != "text/html" ) {
			done( new Error( "Invalid content-type: " + response.headers['content-type'] ) );
			return false;
		}
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testGetAllLimbs = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/all/get'
	};
	request.get( request_options )
    .then( function( response ) {
    	// Convert response to JSON
    	if( typeof response != 'object' )
    		response = JSON.parse( response );

    	if( response.result != "OK" ) {
    		done( new Error( "Result not OK" ) );
    		return false;
    	}
    	
    	if( response.limbs == undefined || Object.keys( response.limbs).length < 0 ) {
    		done( new Error( "Missing limbs" ) );
    		return false;
    	}
    	
    	done();
        return true;
    }).catch( function( err ) {
    	done( new Error( "Unknown Error:" + err ) );
    	return false;
    });
}

var testGetTestLimb = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/get'
	};
	request.get( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		if( typeof response.limb != 'object' ) {
			done( new Error( "Missing limb" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testDeleteAllLimbs = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/all/delete'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testDeleteTestLimb = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/delete'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testClearAllMessageQueues = function( done ) {
	let request_options = {
			url : baseURL + '/all/queue/clear'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testClearBrainMessageQueues = function( done ) {
	let request_options = {
			url : baseURL + '/brain/queue/clear/all'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testClearBrainMessageQueueByLimb = function( done ) {
	let request_options = {
			url : baseURL + '/brain/queue/clear/test'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testClearLimbMessageQueues = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/all/queue/clear'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testClearLimbMessageQueueByLimb = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/queue/clear'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testClearLimbMessageQueue = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/queue/clear'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testBrainBroadcastMessage = function( done ) {
	let request_options = {
			url : baseURL + '/brain/queue/send/all',
			json : true,
			body : {
				action : "REPORT"
			}
	};
	request.put( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}


var testBrainMessageToLimb = function( done ) {
	let params = {
			action : "REBOOT"
	}
	let request_options = {
			url : baseURL + '/brain/queue/send/test',
			json: true,
			body : params
	};
	request.put( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testBrainGetMessage = function( done ) {
	let request_options = {
			url : baseURL + '/brain/queue/get'
	};
	request.get( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK: " + JSON.stringify( response ) ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testLimbGetMessage = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/queue/get'
	};
	request.get( request_options )
    .then( function( response ) {
    	// Convert response to JSON
    	if( typeof response != 'object' )
    		response = JSON.parse( response );
    	
    	if( response.result != "OK" ) {
    		done( new Error( "Result not OK: " + JSON.stringify( response ) ) );
    		return false;
    	}
    	
    	done();
        return true;
    }).catch( function( err ) {
    	done( new Error( "Unknown Error:" + err ) );
    	return false;
    });
}

var testLimbGetNoMessage = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/queue/get'
	};
	request.get( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "NOMSG" ) {
			done( new Error( "Result not NOMSG: " + JSON.stringify( response ) ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testLimbMessageToBrain = function( done ) {
	let params = {
			status : "online",
			ip : "172.16.0.1"
	}
	let request_options = {
			url : baseURL + '/limbs/test/queue/send',
			json: true,
			body : params
	};
	request.put( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testGetTime = function( done ) {
	let request_options = {
			url : baseURL + '/time/get'
	};
	request.get( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testUpdateLimbStatus = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/status/update',
			json : true,
			form : {
				status : "online",
				switch1 : "on",
				switch2 : "off",
				testkey : true
			}
	};
	request.post( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		debug( err );
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testGetLimbStatus = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/status/get'
	};
	request.get( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		debug( err );
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testClearLimbStatus = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/status/clear/all'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		debug( err );
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

var testClearLimbStatusKeyTest = function( done ) {
	let request_options = {
			url : baseURL + '/limbs/test/status/clear/testkey'
	};
	request.delete( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		done();
		return true;
	}).catch( function( err ) {
		debug( err );
		done( new Error( "Unknown Error:" + err ) );
		return false;
	});
}

// Do the tests

describe( 'Do the basic tests:', function() {
	
	it( "serve a page on /", testGetRoot );
	
	it( "get all the limbs", testGetAllLimbs );

	it( "get the 'test' limb", testGetTestLimb );
	
	it( "clear all message queues", testClearAllMessageQueues );
	
	it( "clear brain message queues", testClearBrainMessageQueues );
	
	it( "clear limb message queues", testClearLimbMessageQueues );
	
	it( "remove from brain message queues by limb", testClearBrainMessageQueueByLimb );

	it( "remove from limb message queues by limb", testClearLimbMessageQueueByLimb );

	it( "the brain sends a broadcast message", testBrainBroadcastMessage );
	
	it( "the limb gets the broadcast message", testLimbGetMessage );

	it( "the brain sends a message to 'test'", testBrainMessageToLimb );
	
	it( "the limb gets the directed message", testLimbGetMessage );
	
	it( "the limb gets an empty message", testLimbGetNoMessage );

	it( "the limb sends a new message", testLimbMessageToBrain );
	
	it( "the brain gets the new message from the limb", testBrainGetMessage );
	
	it( "delete the 'test' limb", testDeleteTestLimb );
	
	it( "delete all the limbs", testDeleteAllLimbs );
	
	it( "get the time", testGetTime );
	
	it( "update limb status", testUpdateLimbStatus );
	
	it( "get limb status", testGetLimbStatus );
	
	it( "clear limb status", testClearLimbStatus );
	
	it( "clear limb status key 'test'", testClearLimbStatusKeyTest );

	it( "clear limb message queues", testClearLimbMessageQueues );
	
});

var testReceiveSocketBrainMessage = function( done ) {
	var io = require('socket.io-client');
	
	var mocket = io.connect( baseURL, websocket_options );
	
	mocket.on('connect', function(socket){
		mocket.on('message', function( data ) {
			if( data.message.message == 'test' ) {
				done()
			} else {
				done( new Error( "Something went wrong" ) );
			}
		} );
	});
	
	let params = {
		message : "test"
	}
	
	let request_options = {
			url : baseURL + '/limbs/test/queue/send',
			json: true,
			body : params
	};
	
	request.put( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

var testReceiveSocketStatus = function( done ) {
	var io = require('socket.io-client');
	
	var mocket = io.connect( baseURL, websocket_options );
	
	mocket.on('connect', function(socket){
		mocket.on('status', function( data ) {
			debug( "testReceiveSocketStatus", data );
			if( typeof data.status == 'object' && data.status.message == 'test' ) {
				done()
			} else {
				done( new Error( "Something went wrong" ) );
			}
		} );
	});
	
	let params = {
			message : "test"
	}
	
	let request_options = {
			url : baseURL + '/limbs/test/status/update',
			json: true,
			body : params
	};
	
	request.post( request_options )
	.then( function( response ) {
		// Convert response to JSON
		if( typeof response != 'object' )
			response = JSON.parse( response );
		
		if( response.result != "OK" ) {
			done( new Error( "Result not OK" ) );
			return false;
		}
		
		return true;
	}).catch( function( err ) {
		done( new Error( "Unknown Error: " + err ) );
		return false;
	});
}

describe( 'Do the socket tests:', function() {
	it( "try to receive a message to the brain", testReceiveSocketBrainMessage );
	
	it( "try to receive a status update to the brain", testReceiveSocketStatus );
	
	it( "delete the 'test' limb", testDeleteTestLimb );
});
